(function ($) {

$(document).ready(function(){
	if ($('.flexslider').length) {
		$('.flexslider').flexslider({
			directionNav:true,
			manualControls: "#slider-nav li",
		});
	}
	
});

/*
function setHover() {
	$('header nav ul li').append('<div class="nav-overlay"></div>');
	$('header nav ul li').directionalHover({

	  // CSS class for the overlay
	  overlay: "nav-overlay",

	  // Linear or swing
	  easing: "swing",

	  // <a href="http://www.jqueryscript.net/animation/">Animation</a> speed in ms
	  speed: 400
	  
	});
}
*/

$(function () {

    var $window = $(window);        //Window object

    var scrollTime = 0.4;           //Scroll time
    var scrollDistance = 170;       //Distance. Use smaller value for shorter scroll and greater value for longer scroll

    $window.on("mousewheel DOMMouseScroll", function (event) {

        event.preventDefault();

        var delta = event.originalEvent.wheelDelta / 120 || -event.originalEvent.detail / 3;
        var scrollTop = $window.scrollTop();
        var finalScroll = scrollTop - parseInt(delta * scrollDistance);

        TweenMax.to($window, scrollTime, {
            scrollTo: {y: finalScroll, autoKill: true},
            ease: Power1.easeOut, //For more easing functions see http://api.greensock.com/js/com/greensock/easing/package-detail.html
            autoKill: true,
            overwrite: 5
        });
    });
});



}(jQuery));